#!/usr/bin/env python3

from pathlib import Path
import re
import argparse


#### Arguments en ligne de commande ####

parser = argparse.ArgumentParser()
parser.add_argument("name", default="Fairy Tail", type=str, help="Nom de la série")
parser.add_argument("-s", "--simule", action="count", default=0, help="simule l'execution")
parser.add_argument("-v", "--verbosity", action="count", default=0, help="verbeux ou execution")
parser.add_argument("-l", "--lang", action="count", default=0, help="ajoute la langue")
parser.add_argument("-p", "--quality", action="count", default=0, help="ajoute la résolution")
parser.add_argument("-t", "--title", action="count", default=0, help="ajoute le reste des éléments non extrait au nom du fichier")
parser.add_argument("-x", "--exclude", type=re.compile, help="expression régulière à supprimer du titre avant traitement")

args = parser.parse_args()
serie  = args.name
simule = args.simule > 0
v 	   = args.verbosity
flag_langue = args.lang > 0
flag_quality = args.quality > 0
flag_title = args.title > 0
exclude = args.exclude

#### Extractions dans le nom du fichier ####

numero = re.compile(r'S(?:aison)?(\d+)EP?(?:isodes?)?(\d+)(?:-(\d+))?', re.IGNORECASE) 	# format : ma-série-SxxExxx
numero2 = re.compile(r'(\d+)(?:-(\d+))?')												# format : ma-série-xxx

qualite = re.compile(r'\d{3,4}p', re.IGNORECASE)
langue = re.compile(r'vo(?:stfr)|vf|fr(?:ench)|multi', re.IGNORECASE)
tiret = re.compile(r'\s*-\s+(-\s*)*') # \s+ pour ne pas remplacer les tirets des mots composés ( ex : pomme-de-terre )
# serie+" - S{saison:0>2}E{episode:0>3} - {titre} - {langue} - {qualite}{suffix}"

#### Récupération des fichiers ####

files = [x
	for x in Path('.').iterdir()
	if x.is_file() and numero.search(x.stem if exclude is None else exclude.sub('', x.stem))
]
if not files : # si aucun fichier ne match avec le format ma-série-SxxExxx, on essaye l'autre format.
	numero = numero2
	files = [x
		for x in Path('.').iterdir()
		if x.is_file() and numero2.search(x.stem if exclude is None else exclude.sub('', x.stem))
	]

if v>1 :
	print( "{} fichiers à traiter".format(len(files)) )


#### Traitement ####

for x in files :
	file_name = x.stem if exclude is None else exclude.sub('', x.stem)
	name = file_name.replace(serie, '')
	new_name = [serie]

	# saison et épisode
	search = numero.search(file_name)
	if numero != numero2 :
		(saison,episode,episode2) = search.groups()
	else :
		saison=1 # On suppose être dans la saison 1
		(episode,episode2) = search.groups()

	# choix du format épisode unique ou plage d'épisodes
	saison_episode = "S{0:0>2}E{1:0>3}"
	if episode2 is not None :
		saison_episode += "-{2:0>3}"
	else :
		episode2 = 0 # ne sera pas utilisé dans le format final, mais non None nécessaire pour le cast en int du troisième argument

	# ajoute la saison et le numéro d'épisode
	new_name.append( saison_episode.format(int(saison), int(episode), int(episode2)) )
	name = name.replace(search.group(),'') # suppression de l'élément trouvé

	# qualité
	if flag_quality :
		q = qualite.search(file_name)
		if q :
			new_name.append( q.group() ) # ajoute la qualité si présente (ex : 720p ou 1080p)
			name = name.replace(q.group(),'') # suppression de l'élément trouvé

	# langue
	if flag_langue :
		l = langue.search(file_name)
		if l :
			new_name.append( l.group().upper() ) # ajoute la langue si détectée (vf / vostfr)
			name = name.replace(l.group(),'') # suppression de l'élément trouvé

	# titre
	if flag_title : # insertion du reste du texte (exemmple titre de l'épisode) en deuxième position
		new_name = new_name[0:2] + [name] + new_name[2:]

	# compilation du nouveau nom.
	# 	ajoute des - intermédiaires en fonction des éléments détectés
	# 	supprime les tirets surnuméraire (ainsi que des espaces en trop)

	new_name = tiret.sub(' - ', " - ".join(new_name))
	if new_name[-3:]==' - ' :
		new_name = new_name[:-3]
	new_name = new_name + x.suffix

	# mode simulation ou modification effective du nom.
	if not simule :
		x.rename( new_name )
	if simule or v>0 :
		print( "{} -> {}".format(x.name, new_name) )
