# Renommage séries

Testé sous linux mint python 3.8.10


Permet le renommage des épisodes de séries de façon plus propre.

Détecte le "S1E45" dans le nom, et le reformate avec deux chiffres pour la saison, et trois pour les épisodes ( S1E45 -> S01E045 )

Détecte la langue ( VOSTFR ou VF )

Détecte la résolution (ex 720p ou 1080p)


Formate tout proprement avec un nouveau nom saisi en entré et place les informations avec des tirets et des espaces en fonction des options choisie.

utiliser -h pour plus de détail sur le fonctionnement

